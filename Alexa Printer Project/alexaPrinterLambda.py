"""
This sample demonstrates a simple skill built with the Amazon Alexa Skills Kit.
The Intent Schema, Custom Slots, and Sample Utterances for this skill, as well
as testing instructions are located at http://amzn.to/1LzFrj6

For additional samples, visit the Alexa Skills Kit Getting Started guide at
http://amzn.to/1LGWsLG
"""

from __future__ import print_function
import urllib
import urllib2
import ssl

PRITNER_SERIAL_NUMBER_URL = "https://s3.amazonaws.com/corbin-innovation-alexa-bucket/serialnumber.txt"
PRINT_CONFIG_LABEL_URL = "https://alexa.link-os.com/ZebraWebServicesDeveloperDemo/WebServicesDevDemo/PrintConfig"
PRINT_STATUS_URL = "https://alexa.link-os.com/ZebraWebServicesDeveloperDemo/WebServicesDevDemo/GetStatus"
PRINT_SHIPPING_LABEL_URL = "https://alexa.link-os.com/ZebraWebServicesDeveloperDemo/WebServicesDevDemo/PrintLabel"
PRINT_WEATHER_LABEL_URL = "https://alexa.link-os.com/ZebraWebServicesDeveloperDemo/WebServicesDevDemo/PrintWeather"
# --------------- Helpers that build all of the responses ----------------------

def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': "SessionSpeechlet - " + title,
            'content': "SessionSpeechlet - " + output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }

def get_printer_serial_number():
    request = urllib2.urlopen(PRITNER_SERIAL_NUMBER_URL)
    return request.read()

# --------------- Functions that control the skill's behavior ------------------

def get_welcome_response():
    """ If we wanted to initialize the session to have some attributes we could
    add those here
    """

    session_attributes = {}
    card_title = "Welcome"
    speech_output = "Welcome to the Zebra Printer Alexa Skill.  You can ask me" \
        " to print a shipping label, print a config label, or get status of your" \
        " zebra printer. Just tell me what you want to do."
    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = "You can ask me" \
        " to print a shipping label, print a config label, or get status of your" \
        " zebra printer. Just tell me what you want to do."
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def handle_session_end_request():
    card_title = "Session Ended"
    speech_output = "Thank you for using the Zebra Printer Alexa Skill! Goodbye."
    # Setting this to true ends the session and exits the skill.
    should_end_session = True
    return build_response({}, build_speechlet_response(
        card_title, speech_output, None, should_end_session))

def print_config_label(intent, session):
    """ Prints the printer's config label.
    """
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    printer_serial_number = get_printer_serial_number()

    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE

    method = "POST"
    handler = urllib2.HTTPSHandler(context=ctx)
    opener = urllib2.build_opener(handler)
    data = urllib.urlencode({"SerialNumber": printer_serial_number})
    request = urllib2.Request(PRINT_CONFIG_LABEL_URL, data)
    request.add_header("Content-Type", "application/x-www-form-urlencoded")
    request.get_method = lambda: method
    try:
        connection = opener.open(request)
    except urllib2.HTTPError,e:
        connection = e
    speech_output = "Your config label has successfully printed."
    reprompt_text = ""

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def get_printer_status(intent, session):
    """ Speaks the printer's status.
    """
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    printer_serial_number = get_printer_serial_number()

    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE

    method = "POST"
    handler = urllib2.HTTPSHandler(context=ctx)
    opener = urllib2.build_opener(handler)
    data = urllib.urlencode({"SerialNumber": printer_serial_number})
    request = urllib2.Request(PRINT_STATUS_URL, data)
    request.add_header("Content-Type", "application/x-www-form-urlencoded")
    request.get_method = lambda: method
    try:
        connection = opener.open(request)
    except urllib2.HTTPError,e:
        connection = e
    speech_output = connection.read(1000)
    reprompt_text = ""

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def print_shipping_label(intent, session):
    """ Prints the default shipping label
    """
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    printer_serial_number = get_printer_serial_number()

    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE

    #only if the printer is in a printable state will we print the shipping label.
    method = "POST"
    handler = urllib2.HTTPSHandler(context=ctx)
    opener = urllib2.build_opener(handler)
    data = urllib.urlencode({"SerialNumber": printer_serial_number})
    request = urllib2.Request(PRINT_STATUS_URL, data)
    request.add_header("Content-Type", "application/x-www-form-urlencoded")
    request.get_method = lambda: method

    try:
        connection = opener.open(request)
    except urllib2.HTTPError,e:
        connection = e
    opener.close()
    speech_output = connection.read(1000)
    reprompt_text = ""
    speech_output = speech_output + ". Please clear these errors and retry printing your label."

    if "Your printer is Ready To Print" in speech_output:
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        #if it can print, we print the label.
        speech_output = "Your label has printed."
        method = "POST"
        handler = urllib2.HTTPSHandler(context=ctx)
        opener = urllib2.build_opener(handler)
        data = urllib.urlencode({"SerialNumber": printer_serial_number, "fileName": "exampleshippinglabel.zpl"})
        request = urllib2.Request(PRINT_SHIPPING_LABEL_URL, data)
        request.add_header("Content-Type", "application/x-www-form-urlencoded")
        request.get_method = lambda: method
        try:
            connection = opener.open(request)
        except urllib2.HTTPError,e:
            connection = e

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def print_weather_label(intent, dialog_state, session):
    """ Prints the weather label
    """

    if dialog_state in ("STARTED", "IN_PROGRESS"):
        return continue_dialog()
    elif dialog_state == "COMPLETED":
        zip_code = str(intent['slots']['zipcode']['value'])

        card_title = intent['name']
        session_attributes = {}
        should_end_session = True
        printer_serial_number = get_printer_serial_number()

        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        #only if the printer is in a printable state will we print the shipping label.
        method = "POST"
        handler = urllib2.HTTPSHandler(context=ctx)
        opener = urllib2.build_opener(handler)
        data = urllib.urlencode({"SerialNumber": printer_serial_number})
        request = urllib2.Request(PRINT_STATUS_URL, data)
        request.add_header("Content-Type", "application/x-www-form-urlencoded")
        request.get_method = lambda: method

        try:
            connection = opener.open(request)
        except urllib2.HTTPError,e:
            connection = e
        opener.close()
        speech_output = connection.read(1000)
        reprompt_text = ""
        speech_output = speech_output + ". Please clear these errors and retry printing your label."

        if "Your printer is Ready To Print" in speech_output:
            ctx = ssl.create_default_context()
            ctx.check_hostname = False
            ctx.verify_mode = ssl.CERT_NONE
            #if it can print, we print the label.
            speech_output = "Your label has printed."
            method = "POST"
            handler = urllib2.HTTPSHandler(context=ctx)
            opener = urllib2.build_opener(handler)
            data = urllib.urlencode({"SerialNumber": printer_serial_number, "ZipCode": zip_code})
            request = urllib2.Request(PRINT_WEATHER_LABEL_URL, data)
            request.add_header("Content-Type", "application/x-www-form-urlencoded")
            request.get_method = lambda: method
            try:
                connection = opener.open(request)
            except urllib2.HTTPError,e:
                connection = e

        return build_response(session_attributes, build_speechlet_response(
            card_title, speech_output, reprompt_text, should_end_session))
    else:
        return statement("trip_intent", "No dialog")

def continue_dialog():
    message = {}
    message['shouldEndSession'] = False
    message['directives'] = [{'type': 'Dialog.Delegate'}]
    response = {}
    response['version'] = '1.0'
    response['sessionAttributes'] = {}
    response['response'] = message
    return response
    return response


# --------------- Events ------------------

def on_session_started(session_started_request, session):
    """ Called when the session starts """

    print("on_session_started requestId=" + session_started_request['requestId']
          + ", sessionId=" + session['sessionId'])


def on_launch(launch_request, session):
    """ Called when the user launches the skill without specifying what they
    want
    """

    print("on_launch requestId=" + launch_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # Dispatch to your skill's launch
    return get_welcome_response()


def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """

    print("on_intent requestId=" + intent_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']
    print(intent_request.keys())
    dialog_state = intent_request['dialogState']

    # Dispatch to your skill's intent handlers
    if intent_name == "PrintConfigLabel":
        return print_config_label(intent, session)
    elif intent_name == "GetPrinterStatus":
        return get_printer_status(intent, session)
    elif intent_name == "PrintShippingLabel":
        return print_shipping_label(intent, session)
    elif intent_name == "PrintWeatherLabel":
        return print_weather_label(intent, dialog_state, session)
    elif intent_name == "AMAZON.HelpIntent":
        return get_welcome_response()
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()
    else:
        raise ValueError("Invalid intent")


def on_session_ended(session_ended_request, session):
    """ Called when the user ends the session.

    Is not called when the skill returns should_end_session=true
    """
    print("on_session_ended requestId=" + session_ended_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # add cleanup logic here


# --------------- Main handler ------------------

def lambda_handler(event, context):
    """ Route the incoming request based on type (LaunchRequest, IntentRequest,
    etc.) The JSON body of the request is provided in the event parameter.
    """
    print("event.session.application.applicationId=" +
          event['session']['application']['applicationId'])

    """
    Uncomment this if statement and populate with your skill's application ID to
    prevent someone else from configuring a skill that sends requests to this
    function.
    """
    # if (event['session']['application']['applicationId'] !=
    #         "amzn1.echo-sdk-ams.app.[unique-value-here]"):
    #     raise ValueError("Invalid Application ID")

    if event['session']['new']:
        on_session_started({'requestId': event['request']['requestId']},
                           event['session'])

    if event['request']['type'] == "LaunchRequest":
        return on_launch(event['request'], event['session'])
    elif event['request']['type'] == "IntentRequest":
        return on_intent(event['request'], event['session'])
    elif event['request']['type'] == "SessionEndedRequest":
        return on_session_ended(event['request'], event['session'])
