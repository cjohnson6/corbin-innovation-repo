package com.zebra.devdemo.webservices;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.comm.TcpConnection;
import com.zebra.sdk.printer.PrinterStatus;
import com.zebra.sdk.printer.PrinterStatusMessages;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;
import com.zebra.sdk.remote.comm.RemoteConnection;

@WebServlet("/WebServicesDevDemo/PrintLabelInvoice")
	public class PrintLabelInvoice extends HttpServlet {

		private static final long serialVersionUID = -5602313335397756250L;
		/**
		 * Custom POST implementation which takes in a SerialNumber as the parameter.
		 * A <code>ZebraPrinter</code> object is created and used to print a label with various variables.
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			try {
				//Get the serial number off the request.
				String serialNumber = request.getParameter("SerialNumber");
				String filename = request.getParameter("fileName");
				String boxsize = request.getParameter("boxsize");
				String weight = request.getParameter("weight");
				String invoice = request.getParameter("invoice");
				String label = request.getParameter("label");
				System.out.println("the serial number is " + serialNumber);
				//Create a RemoteConnection on port 11995
				RemoteConnection connection = new RemoteConnection(serialNumber, 11995);
				System.out.println("I have opened a connection at" + connection.toString());
				connection.open();
				
				ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);
				//Decorate the printer as a LinkOsPrinter and print the configuration label.
				PrinterStatus printerStatus = printer.getCurrentStatus();
				
				String toPrint = "";
				if (printerStatus.isReadyToPrint) {
					request.setAttribute("message", "Your printer is Ready To Print");
					request.getRequestDispatcher("/WEB-INF/GetPrinterStatus.jsp").forward(request, response);
					if (label.toLowerCase() == "a")
						{
						toPrint = new Scanner(new URL("https://s3.amazonaws.com/corbin-innovation-alexa-bucket/a.txt").openStream(), "UTF-8").useDelimiter("\\A").next();
						}
					else
					{
						toPrint = new Scanner(new URL("https://s3.amazonaws.com/corbin-innovation-alexa-bucket/b.txt").openStream(), "UTF-8").useDelimiter("\\A").next();
					}
					toPrint = toPrint.replace("$BOXSIZE$", boxsize);
					toPrint = toPrint.replace("$WEIGHT$", weight);
					toPrint = toPrint.replace("$INVOICE$", invoice);
					printer.sendCommand(toPrint);
	            } else {
	                PrinterStatusMessages statusMessage = new PrinterStatusMessages(printerStatus);
	                String[] statusMessages = statusMessage.getStatusMessage();
	                String joinedStatusMessage = "The printer is currently experiencing ";
	                for (int i = 0; i < statusMessages.length; i++) {
	                	if ((statusMessages.length > 1) && (i == (statusMessages.length - 1))) {
	                		joinedStatusMessage += "and ";
	                	}
	                	if("aeiouy".contains(statusMessages[i].substring(0,1).toLowerCase())) {
	                		joinedStatusMessage += "an " + statusMessages[i] + " alert, "; 
	                	}
	                	else {
	                		joinedStatusMessage += "a " + statusMessages[i] + " alert, ";
	                	}
	                		
	                	}
					request.setAttribute("message", joinedStatusMessage);
					request.getRequestDispatcher("/WEB-INF/GetPrinterStatus.jsp").forward(request, response);                	   
	                }

			} catch (ConnectionException e) {
			    e.printStackTrace();
			} catch (ZebraPrinterLanguageUnknownException e) {
		        e.printStackTrace();
			}
		}
		
		
	}
