package com.zebra.devdemo.webservices;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.comm.TcpConnection;
import com.zebra.sdk.printer.PrinterStatus;
import com.zebra.sdk.printer.PrinterStatusMessages;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;
import com.zebra.sdk.remote.comm.RemoteConnection;

@WebServlet("/WebServicesDevDemo/FindMyPrinter")
	public class FindMyPrinter extends HttpServlet {

		private static final long serialVersionUID = -5602313335397756250L;
		/**
		 * Custom POST implementation which takes in a SerialNumber as the parameter.
		 * A <code>ZebraPrinter</code> object is created and used to print a label with various variables.
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				//Get the serial number off the request.
				String serialNumber = request.getParameter("SerialNumber");
				
				//Create a RemoteConnection on port 11995
				RemoteConnection connection = new RemoteConnection(serialNumber, 11995);
				System.out.println("I have opened a connection at" + connection.toString());
				try {
					connection.open();

				
				ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);
				//Decorate the printer as a LinkOsPrinter and print the configuration label.
				
				for(int i = 0; i < 100; i++) {
				Thread.sleep(200);
				printer.sendCommand("! U1 setvar \"display.backlight\" \"off\"\r\n");
				printer.sendCommand("! U1 BEEP 16\r\n");
				Thread.sleep(200);
				printer.sendCommand("! U1 setvar \"display.backlight\" \"on\"\r\n");
				}
	           
				} catch (ConnectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ZebraPrinterLanguageUnknownException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		
	}
}
