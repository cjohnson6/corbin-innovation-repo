package com.zebra.devdemo.webservices;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.comm.TcpConnection;
import com.zebra.sdk.printer.PrinterStatus;
import com.zebra.sdk.printer.PrinterStatusMessages;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;
import com.zebra.sdk.remote.comm.RemoteConnection;

@WebServlet("/WebServicesDevDemo/PrintWeather")
	public class PrintWeather extends HttpServlet {

		private static final long serialVersionUID = -5602313335397756250L;
		/**
		 * Custom POST implementation which takes in a SerialNumber as the parameter.
		 * A <code>ZebraPrinter</code> object is created and used to print the weather as a label in a given zip code.
		 * 
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			try {
				//Get the serial number off the request.
				String serialNumber = request.getParameter("SerialNumber");
				System.out.println("the serial number is " + serialNumber);
				String zip = request.getParameter("ZipCode");
				
				
				//let's make the label
		    	String zipcode = zip;
		    	ObjectMapper mapper = new ObjectMapper();
		    	JsonNode cityroot = mapper.readTree(new URL("https://www.zipcodeapi.com/rest/qDZ8xLTZXkhkEmeJNG25M8BzDhfYsezTSz1qlUpjfHDT4SDRyGXlOyVrPbqip0bh/info.json/"+zipcode+"/radians"));
		    	JsonNode root = mapper.readTree(new URL("http://api.openweathermap.org/data/2.5/weather?zip="+ zip + ",us&units=imperial&APPID=b6a2d4461ccad148fac6324271a8adb2"));
		        
		        String city = root.get("name").asText();
		        String state = cityroot.get("state").asText();
		        String overall = root.get("weather").findValuesAsText("description").get(0);
		    	String current_temp = root.get("main").get("temp").asText();
		    	String high_temp = root.get("main").get("temp_max").asText();
		    	String low_temp = root.get("main").get("temp_min").asText();
		    	String pressure = root.get("main").get("pressure").asText();
		    	String humidity = root.get("main").get("humidity").asText();
		    	String wind = root.findValuesAsText("speed").get(0);
		        //sanity
		    	String label = "^XA^XA^CF0,100^FO50,50^FDWeather for $ZIPCODE$ ^FS^CF0,50^FO50,140^FD$CITY$, $STATE$^FS^CF0,40^FO50,200^FDCurrently: $OVERALL$^FS^FO50,250^GB700,1,3^FS^CFE,30^FO50,300^FDCurrent Temperature: $CURRENTTEMP$ F^FS^FO50,340^FDHigh Temperature: $HIGHTEMP$ F^FS^FO50,380^FDLow Temperature: $LOWTEMP$ F^FS^FO50,420^FDWind Speed: $WIND$ MPH^FS^FO50,500^GB700,1,3^FS^FO50,550^FDPressure: $PRESSURE$ hPa^FS^FO50,590^FDHumidity: $HUMIDITY$ %^FS^XZ^FO50,250^GB700,1,3^FS^FX Second section with recipient address and permit information.^FO50,500^GB700,1,3^FS^XZ\r\n"; 
		    	label = label.replace("$ZIPCODE$", zip);
		    	label = label.replace("$CITY$", city);
		    	label = label.replace("$STATE$", state);
		    	label = label.replace("$OVERALL$", overall);
		    	label = label.replace("$CURRENTTEMP$", current_temp);
		    	label = label.replace("$HIGHTEMP$", high_temp);
		    	label = label.replace("$LOWTEMP$", low_temp);
		    	label = label.replace("$WIND$", wind);
		    	label = label.replace("$PRESSURE$", pressure);
		    	label = label.replace("$HUMIDITY$", humidity);
		    	
		    	
				//label made, let's print it.
				//Create a RemoteConnection on port 11995
				RemoteConnection connection = new RemoteConnection(serialNumber, 11995);
				System.out.println("I have opened a connection at" + connection.toString());
				connection.open();
				
				ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);
				//Decorate the printer as a LinkOsPrinter and print the configuration label.
				PrinterStatus printerStatus = printer.getCurrentStatus();
				
				
				if (printerStatus.isReadyToPrint) {
					request.setAttribute("message", "Your printer is Ready To Print");
					request.getRequestDispatcher("/WEB-INF/GetPrinterStatus.jsp").forward(request, response);
					printer.sendCommand(label);
	            } else {
	                PrinterStatusMessages statusMessage = new PrinterStatusMessages(printerStatus);
	                String[] statusMessages = statusMessage.getStatusMessage();
	                String joinedStatusMessage = "The printer is currently experiencing ";
	                for (int i = 0; i < statusMessages.length; i++) {
	                	if ((statusMessages.length > 1) && (i == (statusMessages.length - 1))) {
	                		joinedStatusMessage += "and ";
	                	}
	                	if("aeiouy".contains(statusMessages[i].substring(0,1).toLowerCase())) {
	                		joinedStatusMessage += "an " + statusMessages[i] + " alert, "; 
	                	}
	                	else {
	                		joinedStatusMessage += "a " + statusMessages[i] + " alert, ";
	                	}
	                		
	                	}
					request.setAttribute("message", joinedStatusMessage);
					request.getRequestDispatcher("/WEB-INF/GetPrinterStatus.jsp").forward(request, response);                	   
	                }


			} catch (ConnectionException e) {
			    e.printStackTrace();
			} catch (ZebraPrinterLanguageUnknownException e) {
		        e.printStackTrace();
			}
		}
		
		
	}
