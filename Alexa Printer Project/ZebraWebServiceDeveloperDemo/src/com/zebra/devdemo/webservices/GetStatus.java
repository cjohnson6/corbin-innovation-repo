package com.zebra.devdemo.webservices;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.PrinterStatus;
import com.zebra.sdk.printer.PrinterStatusMessages;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;
import com.zebra.sdk.remote.comm.RemoteConnection;

@WebServlet("/WebServicesDevDemo/GetStatus")
	public class GetStatus extends HttpServlet {

		private static final long serialVersionUID = -5602313335397756250L;
		/**
		 * Custom POST implementation which takes in a SerialNumber as the parameter.
		 * A <code>ZebraPrinter</code> object is created and used to return the printer's status
		 * to the printer.
		 * 
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			try {
				//Get the serial number off the request.
				String serialNumber = request.getParameter("SerialNumber");
				System.out.println("the serial number is " + serialNumber);
				//Create a RemoteConnection on port 11995
				RemoteConnection connection = new RemoteConnection(serialNumber, 11995);
				System.out.println("I have opened a connection at" + connection.toString());
				connection.open();
				
				ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);
				//Decorate the printer as a LinkOsPrinter and print the configuration label.
				PrinterStatus printerStatus = printer.getCurrentStatus();
				
				
				if (printerStatus.isReadyToPrint) {
					request.setAttribute("message", "Your printer is Ready To Print");
					request.getRequestDispatcher("/WEB-INF/GetPrinterStatus.jsp").forward(request, response);
	            } else {
	                PrinterStatusMessages statusMessage = new PrinterStatusMessages(printerStatus);
	                String[] statusMessages = statusMessage.getStatusMessage();
	                String joinedStatusMessage = "The printer is currently experiencing ";
	                for (int i = 0; i < statusMessages.length; i++) {
	                	if ((statusMessages.length > 1) && (i == (statusMessages.length - 1))) {
	                		joinedStatusMessage += "and ";
	                	}
	                	if("aeiouy".contains(statusMessages[i].substring(0,1).toLowerCase())) {
	                		joinedStatusMessage += "an " + statusMessages[i] + " alert, "; 
	                	}
	                	else {
	                		joinedStatusMessage += "a " + statusMessages[i] + " alert, ";
	                	}
	                		
	                	}
					request.setAttribute("message", joinedStatusMessage);
					request.getRequestDispatcher("/WEB-INF/GetPrinterStatus.jsp").forward(request, response);                	   
	                }

			} catch (ConnectionException e) {
			    e.printStackTrace();
			} catch (ZebraPrinterLanguageUnknownException e) {
		        e.printStackTrace();
			}
		}
		
		
	}
